package App::Scaler::Command::Down;
use strict;
use warnings;
use parent 'App::Scaler';

sub run {
    my ($self) = @_;
    my $count = shift @ARGV;
    if ($count) {
	unless ($count =~ /^\d+$/) {
	    die "integer number expected\n";
        }
        die "too many arguments\n" if @ARGV;
    }
    $self->scale_down($count);
}

1;
