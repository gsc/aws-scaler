package App::Scaler::DB;
use strict;
use warnings;
use GDBM_File;
use File::Path qw(make_path);
use File::Spec;
use App::Scaler::Config;
use Carp;
use App::Scaler::Timestamp;
use JSON;

my %dbtab;

sub new {
    my ($class, $name) = @_;

    my $dir = App::Scaler::Config->new->dbdir;

    unless (-d $dir) {
	make_path($dir) or croak "can't create directory $dir: $!";
    }

    my $filename = File::Spec->catfile($dir, $name);
    if (my $db = $dbtab{$filename}) {
	return new App::Scaler::DB::Ref($db);
    }

    my $self = bless { filename => $filename }, $class;
    
    $self->{dbh} = tie %{$self->{dbhash}}, 'GDBM_File', $filename,
			     GDBM_WRCREAT, 0600
	or croak "can't open database file $filename: $!";
    $dbtab{$filename} = $self;
    $self
}

sub DESTROY {
    my ($self) = @_;
    undef $self->{dbh};
    untie %{$self->{dbhash}};
    delete $dbtab{$self->filename};
}

# See https://rt.perl.org/Public/Bug/Display.html?id=61912.
# sub CLONE_SKIP { 1 }

sub filename { shift->{filename} }

sub fetch {
    my ($self, $key) = @_;
    return unless exists $self->{dbhash}{$key};
    JSON->
	new->
	filter_json_object(sub { timestamp_deserialize $_[0] })->
	decode($self->{dbhash}{$key});
}

sub store {
    my ($self, $key, $val) = @_;
    $self->{dbhash}{$key} = JSON->new->allow_nonref->convert_blessed(1)->encode($val);
}

sub first {
    my ($self) = @_;
    $self->{lastkey} = $self->{dbh}->FIRSTKEY;
}

sub next {
    my ($self) = @_;
    return unless $self->{lastkey};
    $self->{lastkey} = $self->{dbh}->NEXTKEY($self->{lastkey});
}

sub keys {
    my ($self) = @_;
    return keys %{$self->{dbhash}}
}

sub values {
    my ($self) = @_;
    map { $self->fetch($_) } $self->keys;
}

sub delete {
    my ($self, $key) = @_;
    delete $self->{dbhash}{$key};
}
	

package App::Scaler::DB::Ref;
use strict;
use warnings;
use Carp;

sub new {
    my ($class, $db) = @_;
    bless { db => $db }, $class;
}

sub db { shift->{db} }

our $AUTOLOAD;

sub AUTOLOAD {
    my $self = shift;
    (my $meth = $AUTOLOAD) =~ s/.*:://;
    confess qq{Can't locate object method "$meth" via package "$self"}
        unless $self->db->can($meth);
    $self->db->${ \$meth } (@_);
}

1;

    
	
    
    

