package App::Scaler::DB::Instance;
use strict;
use warnings;
use parent 'App::Scaler::DB';

sub new {
    my $class = shift;
    $class->SUPER::new('scaler.db');
}

sub store {
    my ($self, $inst) = @_;
    $self->SUPER::store($inst->{PrivateIpAddress}, $inst);
}

sub delete {
    my ($self, $key) = @_;
    if (ref($key) eq 'HASH') {
	$key = $key->{PrivateIpAddress};
    }
    $self->SUPER::delete($key);
}

1;

    
