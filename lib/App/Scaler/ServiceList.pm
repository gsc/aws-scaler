package App::Scaler::ServiceList;
use strict;
use warnings;
use JSON;
use App::Scaler::Service;

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    open(my $fd, '-|', 'docker', 'service', 'ls', '--format', '{{ json . }}')
	or die "can't start docker: $!";
    while (<$fd>) {
	chomp;
        my $val = JSON->new->decode($_);
	$self->{services}{$val->{Name}} = new App::Scaler::Service($val);
    }
    close $fd;
    $self;
}

sub services { values %{shift->{services}} }

sub service {
    my ($self, $name) = @_;
    return $self->{services}{$name};
}

1;
	

    
