package App::Scaler::Config;
use strict;
use warnings;
use JSON;
use Carp;
use App::Scaler::Base qw(keyfile image subnet instance_type region dbdir domain services tags shutdown_port toss_instance iam_role global);
    
my $config_file = '/etc/scaler.json';

sub new {
    my $class = shift;
    open(my $fd, '<', $config_file)
	or croak "can't open $config_file for reading: $!";
    local $/;
    my $ref = JSON->new->decode(<$fd>);
    close $fd;
    $ref->{dbdir} //= '/var/lib/scaler';
    $ref->{shutdown_port} //= 9889;
    $ref->{toss_instance} //= "oldest";
    bless $ref, $class;
}

1;


