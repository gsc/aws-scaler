package App::Scaler::Node;
use strict;
use warnings;
use App::Scaler::Base qw(Availability EngineVersion Hostname ID ManagerStatus Self Status TLSStatus);

sub new {
    my ($class, $ref) = @_;
    bless $ref, $class;
}

sub ps {
    my ($self) = @_;

    delete $self->{ps};
    open(my $fd, '-|', 'docker', 'node', 'ps', '--format', '{{ json . }}',
	 $self->ID)
	or die "can't start docker: $!";
    while (<$fd>) {
	chomp;
        my $val = JSON->new->decode($_);
	if ($val->{CurrentState} =~ /^Running/) {
	    $self->{ps}{$val->{ID}} = $val;
	}
    }
    close $fd;
    return keys %{$self->{ps}};
}

sub drain {
    my ($self, $wait) = @_;
    system("docker node update --availability drain " . $self->ID);
    if ($wait) {
	while ($self->ps > 0) {
	    sleep(1);
	}
    }
}

sub activate {
    my ($self) = @_;
    system("docker node update --availability active " . $self->ID);
}

1;
