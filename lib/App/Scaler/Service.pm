package App::Scaler::Service;
use strict;
use warnings;
use App::Scaler::Base qw(ID Image Mode Name Ports Replicas);
    
sub new {
    my ($class, $ref) = @_;
    bless $ref, $class;
}

sub current_replicas {
    my $self = shift;
    if ($self->{Mode} eq 'replicated') {
	return (split '/', $self->Replicas)[0];
    }
}

sub max_replicas {
    my $self = shift;
    if ($self->{Mode} eq 'replicated') {
	return (split '/', $self->Replicas)[1];
    }
}    

1;
