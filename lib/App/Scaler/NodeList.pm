package App::Scaler::NodeList;
use strict;
use warnings;
use JSON;
use Carp;
use Socket;
use App::Scaler::Node;

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    $self->update;
    $self;
}

sub update {
    my $self = shift;
    delete $self->{nodes};
    open(my $fd, '-|', 'docker', 'node', 'ls', '--format', '{{ json . }}')
	or die "can't start docker: $!";
    while (<$fd>) {
	chomp;
        my $val = JSON->new->decode($_);
	$self->{nodes}{$val->{ID}} = App::Scaler::Node->new($val);
    }
    close $fd;
}

sub nodes {
    my ($self, %filter) = @_;
    if (keys %filter) {
	grep {
	    my $res = 1;
	    foreach my $k (keys %filter) {
		if ($_->{$k} ne $filter{$k}) {
		    $res = 0;
		    last;
		}
	    }
	    $res	
        } values %{$self->{nodes}}
    } else {
	values %{$self->{nodes}}
    }
}

sub node {
    my ($self, $name) = @_;
    return $self->{services}{$name};
}

sub prepare {
    my ($self, %filter) = @_;
    $self->{filter} = \%filter;
    $self->{start_count} = $self->nodes(%filter);
}

sub wait {
    my ($self) = @_;
    croak "wait called without prepare" unless $self->{filter};
    my $count;
    while (1) {
	$self->update;
	$count = $self->nodes(%{$self->{filter}});
	last unless $self->{start_count} == $count;
	sleep(1);
    }
    my $res = $count - $self->{start_count};
    $self->{start_count} = $count;
    return $res;
}

sub locate {
    my ($self, $ip) = @_;

    foreach my $node ($self->nodes) {
	my @res = gethostbyname($node->Hostname);
	foreach my $a (map { inet_ntoa($_) } @res[4 .. $#res]) {
	    return $node if $a eq $ip;
	}
    }
}

1;
