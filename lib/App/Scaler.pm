package App::Scaler;
use strict;
use warnings;
use App::Scaler::Config;
use App::Scaler::UserData;
use App::Scaler::DB::Instance;
use App::Scaler::ServiceList;
use App::Scaler::NodeList;
use Carp qw(croak cluck);
use IO::Socket::INET;
use App::Scaler::Base qw(dry_run verbose cf);

sub new {
    my ($class, %param) = @_;
    my $self = bless { cf => new App::Scaler::Config,
	    dry_run => $param{dry_run},
	    verbose => $param{verbose}
    }, $class;
    if ($self->{dry_run}) {
	$self->{verbose}++;
    }
    $self
}

sub info {
    my ($self, $level, @rest) = @_;
    if (($self->verbose//0) >= $level) {
	print "DEBUG: " . join(' ', @rest) . "\n";
    }
}

sub runinst {
    my ($self, $num) = @_;    

    $num //= 1;
    
    my $user_data = new App::Scaler::UserData(
        $self->cf->domain,
	$self->cf->keyfile,
	map { $_ => $self->cf->services->{$_}{port} } keys %{$self->cf->services}
    );

    my %spec = (
	'ImageId' => $self->cf->image,
        'InstanceType' => $self->cf->instance_type,
        'MaxCount' => int($num),
        'MinCount' => int($num),
        'SubnetId' => $self->cf->subnet,
        'UserData' => "$user_data",
        'InstanceInitiatedShutdownBehavior' => 'terminate'
	);
    if (my $role = $self->cf->iam_role) {
	$spec{IamInstanceProfile}{Name} = $role;
    }
    if ($self->cf->tags) {
	my $tags = [map { { Key => $_, Value => $self->cf->tags->{$_} } }
	             keys %{$self->cf->tags}];
	$spec{TagSpecifications} = [
	    {
		'ResourceType' => 'instance',
 	        'Tags' => $tags
	    },
	    {
		'ResourceType' => 'volume',
 	        'Tags' => $tags
	    }
        ];
    }
    my $input = JSON->new->encode(\%spec);

    my @cmd = ( 'aws', 'ec2', '--region='.$self->cf->region, 'run-instances',
		'--cli-input-json', $input );
    $self->info(1, "running", @cmd);
    return if $self->dry_run;
    open(my $fd, '-|', @cmd)
	or die "can't start instance";
    local $/;
    my $json = <$fd>;
    unless (close($fd)) {
	croak "run-instances failed: $?, $json";
    }
    foreach my $instance (@{JSON->new->decode($json)->{Instances}}) {
	App::Scaler::DB::Instance->new->store($instance);
        $self->info(0, "Started instance $instance->{InstanceId} at $instance->{PrivateIpAddress}");
    }
}

sub login {
    my ($self) = @_;
    $self->info(0, "logging in to registry",$self->cf->region);
    open(my $fd, '-|',
    	 qw(aws ecr get-login --no-include-email --region),
    	 $self->cf->region)
    	or croak "can't run aws ecr: $!";
    chomp(my $cmd = <$fd>);
    close $fd;
    return if $self->dry_run;
    $self->info(1, "Running command $cmd");
    system($cmd);
    return !$?;
}

sub scale_up {
    my ($self, $incr) = @_;

    $incr //= 1;
	  
    my $srvlist = new App::Scaler::ServiceList;
    my @srvnames = map {
	$self->cf->services->{$_}{name}
    } keys %{$self->cf->services};

    my $nl = new App::Scaler::NodeList;
    $nl->prepare(Status => 'Ready');
    $self->runinst($incr);
    $nl->wait() unless $self->dry_run;
    
    $self->login();
    foreach my $srv (@srvnames) {
        my @cmd = qw(docker service update --detach --with-registry-auth);
	if (!$self->cf->global) {
	    my $n = $srvlist->service($srv)->max_replicas + $incr;
	    $self->info(0, "scaling $srv to $n");
	    push @cmd, "--replicas=$n";
	}
	push @cmd, $srv;
	$self->info(1, "running", @cmd);
	system("@cmd") unless $self->dry_run
    }
    return 0;
}

sub shutdown {
    my ($self, $ip) = @_;

    $self->info(0, "shutting down $ip");
    return if $self->dry_run;
    
    my $sock = IO::Socket::INET->new(PeerAddr => $ip,
				     PeerPort => $self->cf->{shutdown_port});

    croak "Can't connect to $ip:".$self->cf->{shutdown_port}.": $!"
	unless $sock;

    local $/ = "\r\n";
    print $sock "shutdown\r\n";
    chomp(my $resp = <$sock>);
    close $sock;

    if ($resp eq 'SHUTDOWN') {
	print "$ip shut down successfully\n";
	return 1;
    } else {
	print "$ip refused to shut down: $resp\n";
	return 0;
    }
}

sub instance_random {
    my ($self) = @_;
    my $idb = new App::Scaler::DB::Instance;
    if (my $key = $idb->first) {
	return $idb->fetch($key);
    }
}

sub instance_oldest {
    my ($self) = @_;
    my $idb = new App::Scaler::DB::Instance;
    (sort { $a->{LaunchTime} <=> $b->{LaunchTime} }  $idb->values)[0];
}

sub instance_latest {
    my ($self) = @_;
    my $idb = new App::Scaler::DB::Instance;
    (sort { $b->{LaunchTime} <=> $a->{LaunchTime} }  $idb->values)[0];
}

sub toss_instance {
    my ($self) = @_;
    my $meth = "instance_" . $self->cf->toss_instance;
    croak "invalid toss_instance setting: ". $self->cf->toss_instance
	unless $self->can($meth);
    $self->${ \$meth };
}

# Algorithm:

# [ 1) Compute new number of replicas for each service ]

# 2) Downscale the services
#   "docker service update --with-registry-auth --replicas=$n $srv"

# 3) Select a node to shutdown
#    my $x = $self->toss_instance;
#    print "Preparing $x->{InstanceId} ($x->{PrivateIpAddress}) for shutdown\n";

# 4) Find its ID in docker node ls output
#   {"Availability":"Active",
#    "EngineVersion":"20.10.8",
#    "Hostname":"ip-172-17-1-25",
#    "ID":"l8m3ubw3q8nd71oirh1m2p109",
#    "ManagerStatus":"",
#    "Self":false,
#    "Status":"Ready",
#    "TLSStatus":"Ready"}
#  NOTE: Make sure to resolve Hostname for comparizon"

# 5) Switch node to DRAIN mode
#     docker node update --availability drain ID

# 6) Wait until "docker node ps ID" gives empty output.

# 7) Send shutdown command to the node
#   $self->shutdown($ip)

# FINIS
sub scale_down {
    my ($self, $count) = @_;

    $count //= 1;

    my $srvlist = new App::Scaler::ServiceList;
    my @srvnames = map {
    	$self->cf->services->{$_}{name}
    } keys %{$self->cf->services};

    if (!$self->cf->global) {
	# Downscale the services
	foreach my $srv (@srvnames) {
		my $n = $srvlist->service($srv)->max_replicas - $count;
	    	$n = 0 if $n < 0;
		$self->info(0, "scaling $srv down to $n");
		my @cmd = (qw(docker service update --detach), "--replicas=$n", $srv);
		$self->info(1, "running", @cmd);
	        system(@cmd) unless $self->dry_run;
	}
    }

    for (my $i = 0; $i < $count; ) {
	# Select a node to shutdown
        my $inst = $self->toss_instance;
	if (!$inst) {
	    $self->info(0,"instance not found");
	    $i++;
	    next;
	}
        $self->info(0, "Preparing $inst->{InstanceId} ($inst->{PrivateIpAddress}) for shutdown");
        # Locate instance in the node list
        my $nl = new App::Scaler::NodeList;
        my $node = $nl->locate($inst->{PrivateIpAddress});

	if ($node) {
	    $self->info(0, "Draining node", $node->ID);
	    # Switch node to DRAIN mode and wait until fully drained
	    $node->drain(!$self->dry_run);

	    # Shut down the node
	    $self->shutdown($inst->{PrivateIpAddress});
	    
	    $i++;
	} else {
	    cluck "no corresponding node for instance $inst->{InstanceId} ($inst->{PrivateIpAddress})\n";
	}

	$self->info(1, "Deleting $inst->{InstanceId}");
        App::Scaler::DB::Instance->new->delete($inst) unless $self->dry_run;
    }
}

1;
