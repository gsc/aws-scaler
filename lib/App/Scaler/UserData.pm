package App::Scaler::UserData;
use strict;
use warnings;
use Carp;
use JSON -convert_blessed_universally;

sub read_token {
    open(my $fd, '-|', 'docker swarm join-token worker')
	or croak "docker failed";
    local $/;
    my $token;
    if ((my $t = <$fd>) =~ s{.*--token\s+(.+?)\n.*}{$1}ms) {
	$token = $t;
    }
    close $fd;
    return $token;
}

sub read_keyfile {
    my ($self, $filename) = @_;
    open(my $fd, '<', $filename)
	or croak "can't open keyfile $filename: $!";
    while (<$fd>) {
	chomp;
	if (/^key\s+(\S++)\s*\{/) {
	    $self->{key} = $1;
	} elsif (/^\s*(algorithm|secret)\s+(\S+)\s*;/) {
	    ($self->{$1} = $2) =~ s{^"(.+)"$}{$1};
	}
    }
    close $fd;
}

sub new {
    my ($class, $domain, $keyfile, %services) = @_;
    my $self = bless { domain => $domain, services => \%services }, $class;
    $self->{token} = read_token();
    croak "can't extract token\n" unless $self->{token};
    unless (($self->{nameserver} = $self->{token}) =~ s{.*\s(\S+?):\d+}{$1}) {
	croak "can't extract nameserver\n";
    }
    $self->read_keyfile($keyfile);
    $self;
}

sub as_json {
    my ($self) = @_;
    JSON->new->allow_nonref->convert_blessed(1)->canonical->pretty->encode($self);
}
    
use overload
    '""' => \&as_json;

1;
