package App::Scaler::Command::Up;
use strict;
use warnings;
use parent 'App::Scaler';

sub run {
    my ($self) = @_;
    my $count = shift @ARGV;
    if ($count) {
	unless ($count =~ /^\d+$/) {
	    die "integer number expected\n";
        }
        die "too many arguments\n" if @ARGV;
    }
    $self->scale_up($count);
}

1;
